const { Truck } = require('./models/Trucks.js');
const jwt = require('jsonwebtoken');

const truckTypes = {
  SPRINTER: {
    dimensions: {
      width: 300,
      length: 250,
      height: 170
    },
    payload: 1700
  },
  SMALL_STRAIGHT: {
    dimensions: {
      width: 500,
      length: 250,
      height: 170
    },
    payload: 2500
  },
  LARGE_STRAIGHT: {
    dimensions: {
      width: 700,
      length: 350,
      height: 200
    },
    payload: 4000
  }
};

const checkSize = (dim, payload, truck) => {
  if (dim.width <= truck.dimensions.width && dim.length <= truck.dimensions.length
    && dim.height <= truck.dimensions.height && payload <= truck.payload) {
    return true;
  } else {
    return false;
  }
}

const authJWT = (auth) => {
  if (!auth) {
    return res.status(401).json({ 'message': 'Please, provide authorization header' });
  }
  const [, token] = auth.split(' ');
  if (!token) {
    return res.status(401).json({ 'message': 'Please, include token to request' });
  }

  try {
    const tokenPayload = jwt.verify(token, 'secret-jwt-key');
    return tokenPayload;
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
}

const getUserTrucks = async (req, res, next) => {
  const { authorization } = req.headers;
  const user = authJWT(authorization);
  const type = req.body.type;

  if (user.role === "DRIVER") {
    await Truck.find({ created_by: user.userId, type: type })
      .then((result) => {
        return res.status(200).json({ 'trucks': result });
      })
      .catch(err => next(err));
  } else return res.status(400).json({ 'message': 'User must be DRIVER' });
}

const addUserTruck = async (req, res, next) => {
  const type = req.body.type;
  const { authorization } = req.headers;
  const user = authJWT(authorization);
  const currentDate = new Date().toString();

  if (user.role === "DRIVER") {
    const truck = new Truck({
      created_by: user.userId,
      assigned_to: null,
      type: type,
      status: 'IS',
      created_date: currentDate
    });

    await truck.save()
      .then(saved => res.status(200).json({ 'message': 'Truck created successfully',
      'truck': truck
     }))
      .catch(err => next(err));
  } else return res.status(400).json({ 'message': 'User must be DRIVER' });
}

const getUserTruckById = async (req, res, next) => {
  await Truck.findOne({ _id: req.params.id })
    .then((truck) => {
      const thisTruck = truck;
      res.status(200).json({ 'truck': thisTruck });
    });
}

const updateUserTruckById = async (req, res, next) => {
  const type = req.body.payload;
  const { authorization } = req.headers;
  const user = authJWT(authorization);

  if (user.role === 'DRIVER') {
    await Truck.findOneAndUpdate({ _id: req.params.id, created_by: user.userId, status: 'IS' }, { $set: { type: type } })
      .then(truck => res.status(200).json({ 'message': 'Truck details changed successfully' }))
      .catch(err => res.status(400).json({ 'message': 'Truck details not changed' }));
  } else {
    return res.status(400).json({ 'message': 'User must be DRIVER and not OnLoad status' });
  }
}

const deleteUserTruckById = async (req, res, next) => {
  const { authorization } = req.headers;
  const user = authJWT(authorization);

  if (user.role === 'DRIVER') {
    await Truck.findOneAndDelete({ _id: req.params.id, created_by: user.userId, assigned_to: null },
      (truck) => res.status(200).json({ 'message': 'Truck deleted successfully' }));
  } else return res.status(400).json({ 'message': 'User must be DRIVER' })
}

const assignUserTruckById = async (req, res, next) => {
  const { authorization } = req.headers;
  const user = authJWT(authorization);

  if (user.role === 'DRIVER') {
    await Truck.findOneAndUpdate({ _id: req.params.id, created_by: user.userId }, { $set: { assigned_to: user.userId } })
      .then(truck => res.status(200).json({ 'message': 'Truck assigned successfully' }))
      .catch(err => res.status(400).json({ 'message': 'Truck not assigned' }));
  } else {
    return res.status(400).json({ 'message': 'User must be DRIVER' });
  }
}

const findTruckForShipper = async (dimensions, payload) => {
  let truckType;
  if (checkSize(dimensions, payload, truckTypes.SPRINTER)) {
    truckType = 'SPRINTER';
  } else if (checkSize(dimensions, payload, truckTypes.SMALL_STRAIGHT)) {
    truckType = 'SMALL STRAIGHT';
  } else if (checkSize(dimensions, payload, truckTypes.LARGE_STRAIGHT)) {
    truckType = 'LARGE STRAIGHT';
  } else console.log({ 'message': 'Load is too large or heavy' });

  await Truck.findOne({ type: truckType, assigned_to: !null }, { $set: { status: 'OL' }})
    .then((truck) => {
      return truck.assigned_to; 
    })
    .catch(err => {
      return ('Driver not found');
    });
}

const updateTruckForNextShipping = async (driverId) => {
  await Truck.findOneAndUpdate({ assigned_to: driverId }, { $set: { status: 'IS', assigned_to: null } })
    .then(truck => truck.save())
    .catch(err => next(err));
}

module.exports = {
  getUserTrucks,
  addUserTruck,
  getUserTruckById,
  updateUserTruckById,
  deleteUserTruckById,
  assignUserTruckById,
  findTruckForShipper,
  updateTruckForNextShipping,
  truckTypes
};
