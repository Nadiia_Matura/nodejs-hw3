const express = require('express');
const router = express.Router();
const {
  getUserTrucks, addUserTruck, getUserTruckById, 
  updateUserTruckById, deleteUserTruckById, assignUserTruckById
} = require('./trucksService.js');

router.get('/trucks', getUserTrucks);
router.post('/trucks', addUserTruck);
router.get('/trucks/:id', getUserTruckById);
router.put('/trucks/:id', updateUserTruckById);
router.delete('/trucks/:id', deleteUserTruckById);
router.post('/trucks/:id/assign', assignUserTruckById);

module.exports = {
  trucksRouter: router
};
