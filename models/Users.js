const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  role: {
    type: String,
    required: true,
    enum: [ 'DRIVER', 'SHIPPER']
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true
  },
  created_date: {
    type: String,
    required: true
  }
}); 

const User = mongoose.model('User', userSchema );

module.exports = {
  User
};
