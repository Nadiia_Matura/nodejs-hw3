const mongoose = require('mongoose');

const truckSchema =  new mongoose.Schema({
  created_by: {
    type: String
  },
  assigned_to: {
    type: String 
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT']
  },
  status: {
    type: String,
    enum: [ 'OL', 'IS']
  },
  created_date: {
    type: String
  }
});

const Truck = mongoose.model('truck', truckSchema);

module.exports = {
  Truck
};
